# Educational Dataset Schema

This repository holds a [Json Schema][json-schema] which defines the metadata supported for data exchange into the UC
San Diego Educational Dataset Repository.

There are also a set of example fixtures included in `fixtures/` which cover various use cases, which may be used for
reference.

[json-schema]:https://json-schema.org/
